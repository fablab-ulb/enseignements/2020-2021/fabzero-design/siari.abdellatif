<br>

---
---
---

| | |
| --- | --- |
| ![Photo](images/Photo.jpg) | Siari Abdellatif tout le monde m'appelle LOTFI. Je suis un étudiant en master 2 à la faculté d'architecture de l'ULB. J'ai choisi pour ce quadrimestre Architcture et Design pour comprendre le fonctionnement des machines pour agrandir mon champ de conception et crée des formes propres au numérique. Sur cette plate-forme vous trouveriez toute l'expérience que j'aurai durant ce quadrimestre et l'évolution de cette question. |

---
---
---

## Mon parcours

Compte tenu de mes aptitudes actuelles relatif à un diplôme de master 2 en « architecture et urbanisme » obtenu en 2018 à l’Université d'Hadj Lakhdher Batna, Algérie. Un nouvel horizon s’ouvre devant moi lequel me pousse à m'inscrire à un master en architecture, pour étendre mes connaissances dans ce domaine et surtout m’imprégner du savoir. je suis venus à Bruxelles l'année passée et maintenant ça fait un ans que je suis ici à Bruxelles.

## Choix de l'objet - La chaise FLORIS de Günter Beltzig 

Le premier point qui m’a attiré sur cette chaise est l’utilisation de 3 pieds au lieu de 4. De là je me suis dit que cette chaise avec cette forme qui est conçue en 1967 est assez futuriste, peut refléter un travail très détaillé. Pour avoir une telle forme dans cette époque, cette chaise ne doit pas seulement refléter l’époque, mais aussi la personnalité du désigner, le vécu et la fonction.

En répondant sur toutes les questions de ce design, on peut répondre aussi au design de la table et de la petite chaise qui l’entoure.

---

| | |
| --- | --- |
|![Photo](images/floris_chair.jpg) | ![Photo](images/floris_chair_1.jpg)|

---

| | |
| --- | --- |
| ![Photo](images/Beltzig_gunter.jpg) | **__Gunter Beltzig__** est né en 1941 en Allemagne.<br><br>Déjà adolescent il aimait s’exprimer à travers le dessin et était plein d’idées et de visions.<br>Son réel désir était de devenir inventeur malgré qu’il ait commencé son apprentissage en tant que monteur de machine.<br>Après avoir étudié le design industriel à la Werkkunstschule Wuppertal il a travaillé dans le département design de Siemens AG à Munich. Mais ce travail ne lui laissait pas un champ de liberté de création très grand. C’est pourquoi, en 1966, il fonda avec son frère la compagnie de plastique Beltzig Design. Ils développèrent des mobiliers et **équipement robustes principalement pour l’extérieur et les plaines de jeux.** Mais également **des meubles d’intérieur** aux formes originales. Ce qu’il voulait, c’était créer une aventure plus qu’une plaine de jeux. Il pensait également beaucoup à l’**ergonomie et la sécurité**.|

## La chaise Floris

La série de meubles « Floris », dont la chaise que j’ai choisie de vous présenter, fut créée en 1967 et rendit leur compagnie connue presque du jour au lendemain.

· Réalisée en **fibre de verre**, ce qui offre une excellente __résistance__ en même temps qu’une __légèreté__. Malgré les atouts de ce matériau la production de cette chaise reste complexe. Elle ne fût donc créée qu’en une cinquantaine d’exemplaires. 

· Mais il faut dire que le fait qu’elle soit **réalisée en une seule pièce** et qu’elle ne **nécessite donc pas d’assemblage**, permet de réduire le coût de fabrication mais aussi d’apporter une plus grande stabilité et durabilité.

· Ces **3 pieds** sont faits pour amener **une stabilité même dans __les endroits où le sol n’est pas plat__**. Ils sont anti basculement et peuvent donc s’adapter plus facilement à différents terrains que si la chaise avait eu 4 pieds.

· Quant au dossier, son **ergonomie** réside dans ses 3 points de supports dont le corps à besoin pour s’asseoir : au niveau du cou, des fesses et du dos. Elle permet un parfait soutient pelvien dû au dossier légèrement flexible.

· De même pour l’**assise** dont l’angle d’inclinaison a été étudié pour épouser la forme naturelle du corps.

· En plus d’être légère et stable, la chaise Floris est également **empilable**.

---
---

· Sa forme organique et pleine de courbes est inspirée des plantes et de la féminité. Mais ces courbes ont aussi une utilité. L’absence de coin et de raccords permet d’éviter l’accumulation de saleté. De plus, la vallée que l’on retrouve dans l’assise et qui va jusqu’aux pieds, permet l’auto drainage et nettoyage de cette chaise d’extérieur lorsqu’il pleut. Elle permet également la ventilation lorsque l’on est assis. La surface des pieds qui est en relation avec le sol est assez large. La raison est que plus cette surface est grande, moins il y aura de pression.

![Photo](images/272.jpg)

---
---

| | | |
| --- | --- | --- |
| ![Photo](images/00.png) | ![Photo](images/01.png) | ![Photo](images/02.png) |

### Bolidisme 

![Photo](images/1442985457209285.jpg)

Un tel design conduit à une symbolique initiale privilégiant les formes dynamiques voulues non seulement comme « façonnées par le vent », mais aussi comme des formes organiques, qui restituent la personnalité et l’unicité typique des formes vivantes, pouvant être industrialisées dans leur complexité et leur diversité grâce à l'évolution de la technologie

### Luigi Colani

![Photo](images/Schimmel_K208Pegasus_1.jpg)

Son travail s'oriente autour de l'homme et de son rapport optimal et efficace à l'objet et à son interaction avec son environnement, les formes sont inspirées de la perfection de la nature elle-même ; et sont loin d'un « effet de style », c'est de conception utile dont son œuvre fait preuve.

### Période

La chaise est concu dans les années 60-70, l’année du **style Pop**. _Pour dynamiser les espaces et accrocher l’œil, tout est permis_.<br><br>
Déboulant dans les années 60, le pop art exprimait un nouvel état d’esprit.<br>
Vague de libération, désir de casser les codes, l’époque vibrionnait et pas seulement dans le domaine artistique. La mode … et bien sûr la décoration furent impactées. Créative et pleine d’audace, la culture pop façonne dans les années 70 de nouvelles manières de vivre car c’est aussi **le moment de l’avènement de la société de consommation et de la publicité.**<br>
Le design se veut accessible à tous et les meubles en matière plastique sortent des usines en série.

#### Style Pop

Avec le style pop, on joue la carte de l'originalité jusque dans le design du mobilier. Les matériaux sont brillants, une touche de métal, des finitions laquées, du PVC, du bois seulement avec parcimonie. Les lignes sont arrondies et vont là où on ne les attend pas. Une bibliothèque ronde, une étagère circulaire, des fauteuils bulle. Sans oublier la touche colorée, qui vient sublimer le mobilier. Les éléments sont souvent assez imposants, de par leur design. On ne joue pas la surenchère, mais on choisit avec soin chaque meuble, pour ne pas surcharger son intérieur.<br><br>

**Tout en courbes, _le style pop arrondit les angles_**. Les formes du mobilier peuvent se montrer extravagantes, comme dans un fauteuil bulle qui s’épanouira au centre de votre pièce, une table Tulip ou des étagères circulaires. Du côté des luminaires et des tables, la rondeur est aussi de mise. Mais le style pop accepte par ailleurs les imprimés et motifs très graphiques, dans un papier peint ou un tapis façon patchwork par exemple.

#### Pop art

L'expression « pop art » (abréviation de « popular art » en anglais, ou « art populaire » en français), a été utilisée pour la première fois par les membres de l'Independent Group, groupe d'intellectuels travaillant sur l'impact des médias de masse et de la technologie dans la société se réunissant à l'Institute of Contemporary Art (ICA) à Londres. <br><br>

Le pop art est un ensemble de phénomènes artistiques intimement liés à l'esprit d'une époque, l'essence d'un large mouvement culturel des années 1960. Il trouve son origine en Grande-Bretagne au milieu des années 1950, mais se répand rapidement à l'ensemble du monde occidentalisé dans le contexte de la société industrielle capitaliste. Celle-ci s'appuie sur les nouvelles technologies en plein essor dont les artistes pop vont s'emparer et qui touchent toute la sphère culturelle : le pop art se manifeste dans les pratiques et les comportements de toute une génération. <br> Mais ce mouvement ne se limite plus seulement au domaine des arts plastiques : il touche autant la musique, la mode et les arts appliqués et bien d'autres domaines de la culture.

#### Matières

Dans ces années, les matières plastiques et résines synthétiques s’invitent partout et peuvent prendre des formes aussi diverses qu’insolites. Dans les meubles et les accessoires, le plastique offre un beau terrain de jeu au design et bénéficie d’un côté pratique, léger, ultra facile d’entretien.



