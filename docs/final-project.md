# Objet final

En reproduisant en dessin la posture  que l’on adopte lorsqu’on est assis sur cette balle je me suis aperçu qu’il était possible de la rendre plus pratique. 

Pour le dernier prototype, j'ai démarré par un dessin basique pour simplifier ma forme, je suis partie par une hauteur de 56 cm d'une façon qu'on peut glisser la chaise sous le bureau, puis multiplier la hauteur par π (Pi) pour avoir le diamètre du cercle et avec une balance agréable, sans oublier que le jeu de la balance est plus correct au centre de gravité, c'est de là où j'ai mis la même courbe dans l'assise comme ça dans chaque point de la courbe a la même hauteur de l'assise.

La chaise offre une balance horizontale, réduit la pression sur la colonne vertébrale, tout en corrigeant la posture et améliorant l'équilibre.

47 - 43 - 39 - 35 - 31 cm c'est les différentes hauteurs qui se trouve dans la chaise, la chaise peut être utilisée par des enfants comme par des adultes avec les différentes tailles.

---

La chaise se compose de 9 piéces

---

![chaise](images/chaise_.jpg)

---

![final](images/final.jpg)

---

### Matériaux: Multiplex, colle blanche, vis.
### Matériels : Scie circulaire, shaper, serre joint.

---

![coupe_shaper](images/coupe_shaper.jpg)


![coupe_shaper](images/assemblage_colle.jpg)


![coupe_shaper](images/assemblage_final.jpg)

---

![coupe_shaper](images/Chaise-final.jpg)

### Recherche

[Planche latérale SVG](Fichiers/planche_latérale.svg) <br>
[Pièces 2D dwg](Fichiers/Pièces_2D.dwg) <br>
[Chaise 3D stl](Fichiers/Chise_3D.stl)


# Recherche


## Choix de l'objet - La chaise FLORIS de Günter Beltzig 

Le premier point qui m’a attiré sur cette chaise est l’utilisation de 3 pieds au lieu de 4. De là je me suis dit que cette chaise avec cette forme qui est conçue en 1967 est assez futuriste, peut refléter un travail très détaillé. Pour avoir une telle forme dans cette époque, cette chaise ne doit pas seulement refléter l’époque, mais aussi la personnalité du désigner, le vécu et la fonction.

En répondant sur toutes les questions de ce design, on peut répondre aussi au design de la table et de la petite chaise qui l’entoure.

---

| | |
| --- | --- |
|![Photo](images/floris_chair.jpg) | ![Photo](images/floris_chair_1.jpg)|

---

| | |
| --- | --- |
| ![Photo](images/Beltzig_gunter.jpg) | **__Gunter Beltzig__** est né en 1941 en Allemagne.<br><br>Déjà adolescent il aimait s’exprimer à travers le dessin et était plein d’idées et de visions.<br>Son réel désir était de devenir inventeur malgré qu’il ait commencé son apprentissage en tant que monteur de machine.<br>Après avoir étudié le design industriel à la Werkkunstschule Wuppertal il a travaillé dans le département design de Siemens AG à Munich. Mais ce travail ne lui laissait pas un champ de liberté de création très grand. C’est pourquoi, en 1966, il fonda avec son frère la compagnie de plastique Beltzig Design. Ils développèrent des mobiliers et **équipement robustes principalement pour l’extérieur et les plaines de jeux.** Mais également **des meubles d’intérieur** aux formes originales. Ce qu’il voulait, c’était créer une aventure plus qu’une plaine de jeux. Il pensait également beaucoup à l’**ergonomie et la sécurité**.|

## La chaise Floris

La série de meubles « Floris », dont la chaise que j’ai choisie de vous présenter, fut créée en 1967 et rendit leur compagnie connue presque du jour au lendemain.

| | |
| --- | --- |
| ![de1](images/schémas_chaise.jpg)|· Réalisée en **fibre de verre**, ce qui offre une excellente __résistance__ en même temps qu’une __légèreté__. Malgré les atouts de ce matériau la production de cette chaise reste complexe. Elle ne fût donc créée qu’en une cinquantaine d’exemplaires. <br>· Mais il faut dire que le fait qu’elle soit **réalisée en une seule pièce** et qu’elle ne **nécessite donc pas d’assemblage**, permet de réduire le coût de fabrication mais aussi d’apporter une plus grande stabilité et durabilité. <br>· Ces **3 pieds** sont faits pour amener **une stabilité même dans __les endroits où le sol n’est pas plat__**. Ils sont anti basculement et peuvent donc s’adapter plus facilement à différents terrains que si la chaise avait eu 4 pieds. <br>· Quant au dossier, son **ergonomie** réside dans ses 3 points de supports dont le corps à besoin pour s’asseoir : au niveau du cou, des fesses et du dos. Elle permet un parfait soutient pelvien dû au dossier légèrement flexible. <br>· De même pour l’**assise** dont l’angle d’inclinaison a été étudié pour épouser la forme naturelle du corps.  <br>· En plus d’être légère et stable, la chaise Floris est également **empilable**. |

---
---

· Sa forme organique et pleine de courbes est inspirée des plantes et de la féminité. Mais ces courbes ont aussi une utilité. L’absence de coin et de raccords permet d’éviter l’accumulation de saleté. De plus, la vallée que l’on retrouve dans l’assise et qui va jusqu’aux pieds, permet l’auto drainage et nettoyage de cette chaise d’extérieur lorsqu’il pleut. Elle permet également la ventilation lorsque l’on est assis. La surface des pieds qui est en relation avec le sol est assez large. La raison est que plus cette surface est grande, moins il y aura de pression.

![Photo](images/272.jpg)

---
---

| | | |
| --- | --- | --- |
| ![Photo](images/00.png) | ![Photo](images/01.png) | ![Photo](images/02.png) |

### Bolidisme 

![Photo](images/1442985457209285.jpg)

Un tel design conduit à une symbolique initiale privilégiant les formes dynamiques voulues non seulement comme « façonnées par le vent », mais aussi comme des formes organiques, qui restituent la personnalité et l’unicité typique des formes vivantes, pouvant être industrialisées dans leur complexité et leur diversité grâce à l'évolution de la technologie

### Luigi Colani

![Photo](images/Schimmel_K208Pegasus_1.jpg)

Son travail s'oriente autour de l'homme et de son rapport optimal et efficace à l'objet et à son interaction avec son environnement, les formes sont inspirées de la perfection de la nature elle-même ; et sont loin d'un « effet de style », c'est de conception utile dont son œuvre fait preuve.

### Période

La chaise est concu dans les années 60-70, l’année du **style Pop**. _Pour dynamiser les espaces et accrocher l’œil, tout est permis_.<br><br>
Déboulant dans les années 60, le pop art exprimait un nouvel état d’esprit.<br>
Vague de libération, désir de casser les codes, l’époque vibrionnait et pas seulement dans le domaine artistique. La mode … et bien sûr la décoration furent impactées. Créative et pleine d’audace, la culture pop façonne dans les années 70 de nouvelles manières de vivre car c’est aussi **le moment de l’avènement de la société de consommation et de la publicité.**<br>
Le design se veut accessible à tous et les meubles en matière plastique sortent des usines en série.

#### Style Pop

Avec le style pop, on joue la carte de l'originalité jusque dans le design du mobilier. Les matériaux sont brillants, une touche de métal, des finitions laquées, du PVC, du bois seulement avec parcimonie. Les lignes sont arrondies et vont là où on ne les attend pas. Une bibliothèque ronde, une étagère circulaire, des fauteuils bulle. Sans oublier la touche colorée, qui vient sublimer le mobilier. Les éléments sont souvent assez imposants, de par leur design. On ne joue pas la surenchère, mais on choisit avec soin chaque meuble, pour ne pas surcharger son intérieur.<br><br>

**Tout en courbes, _le style pop arrondit les angles_**. Les formes du mobilier peuvent se montrer extravagantes, comme dans un fauteuil bulle qui s’épanouira au centre de votre pièce, une table Tulip ou des étagères circulaires. Du côté des luminaires et des tables, la rondeur est aussi de mise. Mais le style pop accepte par ailleurs les imprimés et motifs très graphiques, dans un papier peint ou un tapis façon patchwork par exemple.

#### Pop art

L'expression « pop art » (abréviation de « popular art » en anglais, ou « art populaire » en français), a été utilisée pour la première fois par les membres de l'Independent Group, groupe d'intellectuels travaillant sur l'impact des médias de masse et de la technologie dans la société se réunissant à l'Institute of Contemporary Art (ICA) à Londres. <br><br>

Le pop art est un ensemble de phénomènes artistiques intimement liés à l'esprit d'une époque, l'essence d'un large mouvement culturel des années 1960. Il trouve son origine en Grande-Bretagne au milieu des années 1950, mais se répand rapidement à l'ensemble du monde occidentalisé dans le contexte de la société industrielle capitaliste. Celle-ci s'appuie sur les nouvelles technologies en plein essor dont les artistes pop vont s'emparer et qui touchent toute la sphère culturelle : le pop art se manifeste dans les pratiques et les comportements de toute une génération. <br> Mais ce mouvement ne se limite plus seulement au domaine des arts plastiques : il touche autant la musique, la mode et les arts appliqués et bien d'autres domaines de la culture.

#### Matières

Dans ces années, les matières plastiques et résines synthétiques s’invitent partout et peuvent prendre des formes aussi diverses qu’insolites. Dans les meubles et les accessoires, le plastique offre un beau terrain de jeu au design et bénéficie d’un côté pratique, léger, ultra facile d’entretien.

## Fibre de verre

· La fibre de verre ne coûte pas chère. 
· Large choix de fibres et toiles de verre pour toutes les envies : textures, motifs et matériaux.
· Ce revêtement à peindre est très léger, résistant et se laisse poser sans aucune difficulté. Il existe des astuces faciles pour poser la fibre de verre dans un angle sortant.
· La toile de verre est un isolant léger. Il existe des fibres de verre isolantes pour parfaire son isolation thermique et phonique.
· Une fois posée, la fibre de verre se laisse peindre simplement. La toile de verre peut être repeinte une dizaine de fois, de quoi changer de peinture régulièrement.

# PROJET
La chaise que j’avais choisie au musée Adam avait été créée après une réflexion sur la posture idéale et sur un siège qui apporterait un soutient parfait. 
Par l'analyse de l'objet, j'ai pu comprendre la méthode qui a utilisé le designeur pour concevoir la chaise, comme c'est définie sur ses schémas, chauqe point a une relation avec le confort ou l'utilisation de l'objet, donc son concept a commencé par une idée pratique qui a fini par crée cette objet même. En voyant, se parcours et on parlant avec Victor, m'a orienter vers un autre disgneur "Peter Opsvik" qui utilise la même maniére de conception, comme on peut voir sur son livre "Rethinking Sitting", publié en 2009. Sur ce livre, on peut voir la façon de ça conception, comme exemple la chaise Garden « Depuis l’âge de pierre, l’homme travaille assis dans différentes postures, or la typologie unique depuis le Moyen-âge est la chaise… Nous ne sommes pas faits pour rester figés dans la même position du matin au soir », écrit-il. Par ces mots là, on peut comprendre sa façon de concevoir, "la fonction qui crée le disgne".

---

![Photo](images/Peter_Opsvik_1.jpg)

---

J’ai donc fait des recherches sur les différents sièges qui avaient été conçu pour nous aider à avoir une meilleure posture. C’est là que j’ai trouvé la balle qui est même utilisée dans les bureaux là où l’on reste assis la plupart du temps. Elle aide à se tenir plus droit et à apporter un équilibre à la colonne vertébrale. Un de mes collègues s'assoit toujours sur la bale, donc je lui poser la question, c'est quoi les points negatifs sur cette balle ?

· Ça prend beaucoupe d'espace.<br>
· Je ne peut pas l'utiliser a l'extérieur pour ne pas l'abîmer.<br>
· Je ne peut mettre mes pieds par derriére.<br><br>

Par contre, La chaise ballon réduit la pression sur la colonne vertébrale, tout en corrigeant la posture et améliorant l'équilibre. De plus la chaise ballon renforce les abdominaux et le torse et ainsi réduire les troubles musculosquelettiques (TMS).<br>
![Photo](images/ballon-ergonomique-ball-jaune-safran.jpg) 

On prenant les points cités, je me disais pourquoi ne pas crée une chaise en bois qui a le même rôle de la balle.
 
---

# Prototype 1

En reproduisant en dessin la posture  que l’on adopte lorsqu’on est assis sur cette balle je me suis aperçu qu’il était possible de la rendre plus pratique. Par exemple, j’ai constaté que si l’on retirait les parties courbes  du dessus et des côtés, celle-ci prend alors moins de place.  C’est donc cette direction que j’ai voulu exploiter. 

J’ai ensuite pensé au balancement lorsqu’on s’assoit dessus. Et j’ai alors ajouté à l’arrière une partie droite qui servirait de pied pour ne pas tomber en arrière.  Ce pied ne doit pas être trop court sinon le balancement est trop fort et la chaise n’est plus confortable. De même, s’il est trop long, le balancement est presque inexistant et du coup on perd l’objectif de la chaise. 

J’ai observé que l’angle idéal devrait être de 10°. 

J’ai ensuite constaté qu’il était mieux de mettre un deuxième pied à l’avant, ainsi la chaise pourrait être utilisable de la même façon dans les deux sens. 

![Photo](images/CHAISE.jpg)

En réalisant des schémas 2D, j'ai pu crée la forme globale de la chaise puis je me suis mit sur la 3D pour mieux voir la forme.

![Photo](images/3d2.png)

Par la suite, des essais en carton mon amené a une nouvelle forme plus résistante au niveau des pieds je me suis aperçu qu’il était possible de l’utiliser aussi en mettant ces jambes sur l’autre face avec les pieds de la chaise sur les côtés. Cela permet également de travailler la posture du dos tout en ne risquant pas de tomber à gauche ou à droite. J’ai aussi pu confirmer que la longueur des pieds était bonne car même en laissant le poids du corps sur la gauche ou la droite cela n’était pas inconfortable car le balancement est suffisamment léger. 

![Photo](images/IMG_1575.jpg) ![Photo](images/IMG_1576.jpg)


En commençant les dessins pour faire un prototype en bois j’ai décidé d’y ajouter deux encoches pour faciliter le transport de la chaise d'une façon qu'on peut porter deux avec une seul main.

![Photo](images/Sans_titre-12.jpg)


« Passer moins de temps assis et varier les postures » Rethinking Sitting, Peter Opsvik, 2009.

# Composition d'objet


![Photo](images/découpe2.png)

En voyant ça avec Victor, ma demander si j’ai pris le poids de la chaise en considération, 10 pièces ça fait plus de poids et pour une chaise transportable le mieux est d’avoir une assez légère.

Je me suis dit un test avec du MDF répond à la notion du poids, j’ai commencé à faire la chaise avec 4 pièces pour voir la stabilité et le poids, j’ai mis les planches latérales de 3cm de mois que celle poser sur le sol pour un léger balancement, et j’ai travaillé avec une dimension de 30 x 30 x h 45 cm. 

J’ai réussi faire toutes les découpes avec la Shaper, ça m’a pris plus de temps surtout sur les surfaces droites qui non pas besoin d’une Shaper, mais au final j’étais assez satisfait et en travaillant avec la Shaper, même les ligne droite en était coupé parfaitement en plus je n’ai pas perdu les repères de la Shaper.

![Photo](images/IMG_1647.jpg)

Puis j’ai tout assemblé avec des vises, comme on peut le voir dans l’image qui suit.

### Remarques :
· Niveau de balancement est assez agréable, des fois on ne peut pas contrôler l’angle exact comme on veut, car le demi-cercle est assez petit.<br>
· Les planches latéraux qui bloque le balancement sont un peu haut, donc la chaise s’incline plus qu’il faut.<br>
· Au niveau des fesses c’est pas du tout confortable.<br>
### Réflexions :
· Donner un diamètre plus grand au cercle, nous permet de mieux équilibrer la chaise et avoir plus de point d’angle. <br>
· Au lieu de faire les latéraux de 42 cm, l’idéal c’est de le faire a 43 cm.<br>
· Faire la partie supérieure d’une façon bombée pour qu’elle puisse donner plus de confort.<br>


![Photo](images/tabouret.jpg)

### Conclusion :
Partir d’une conception où n’y aura pas des arrêts (les parties circulaires devront faire l’arrêt à un certain point de balancement). Que les formes circulaire épouse l’assis en haut jusque qu’ils forment un assis agréable. 
Jouer avec la graviter et le poids pour bien positionner le corps a la partie circulaire.

# Prototype 2

Chaise à balance comment est faite pour qu'elle tient toute cette stabilité seulement avec la gravité ?

![Photo](images/1110_cog_rev.jpg)

La chaise influence cet emplacement non seulement par la position de l'assise et du dossier, mais aussi un peu par la répartition du poids de ses composants. Où se trouve le centre de gravité ? En général, le centre de gravité d'une personne assise en position verticale sur une chaise se trouve à environ un centimètre devant le nombril. Mais chaque chaise est différente, et non seulement chaque personne est légèrement différente, mais chaque position assise (pieds étendus, pieds repliés, affaissés, etc.) modifie l'emplacement du centre de gravité.

Un mouvement de balancement typique ne couvre qu'environ 8º à 10º de la bascule. Si vous tenez compte du fait que vous vous êtes installé dans la chaise et que vous l'avez repoussée assez loin, cela peut impliquer tout le 14º des bascules. 

Bien sûr, le rayon de l'arc est important par rapport au centre de gravité. Si le centre de gravité se trouve à peu près à 63,5 ou 66 cm du sol (une chaise à bascule typique l'y placerait), vous pouvez voir qu'un rayon de bascule de seulement 76,2 cm placerait le centre de gravité près du centre du cercle. Cela donne une chaise très basculante. Pourquoi est-ce que je parle de la hauteur du centre de gravité et non de la hauteur du siège ? Les formes et les angles des sièges à bascule peuvent être très différents, et il est difficile de définir un point de référence cohérent et significatif d'une chaise à l'autre.

Tout cela peut permettre de mieux comprendre ce qui se passe avec un rocking chair, mais malgré cela, peu d'informations pratiques sont apparues. Vous devriez cependant comprendre un peu plus clairement pourquoi. Chaque chaise est vraiment différente et, à part quelques paramètres de base, il est difficile d'appliquer des règles strictes pour trouver une solution qui fonctionne. En d'autres termes, il n'y a pas de moyen infaillible de calculer comment tous les différents facteurs vont se rejoindre.

Source : Articl. Make a chair that rocks, Jeff Miller, Avril 2020, Design.

### Dessin 01

---

![Photo](images/80.png) 

---

![Photo](images/81.jpg)

---

![Photo](images/96.jpg)

---

### Dessin 02

---

![Photo](images/97.jpg)

---

### Dessin 03

---

![Photo](images/82.jpg)

---

![Photo](images/98.jpg)

---

### Dessin 04

---

![Photo](images/83.png)


---


![Photo](images/99.jpg)


---

![Photo](images/100.jpg)
![Photo](images/101.png)

---

# Prototype 3

Pour ce prototype, j'ai essayer de jouer sur l'axe horizotal avec une assise en forme cylindrique qui est tenu par une barre en milieu pour faire le circuler le cylindre.

---

![Photo](images/999.png)

---


# Prototype 4

### Dessin 01

---

![PROTOTYPE_4-1](images/PROTOTYPE_4-1.jpg)

---

### Dessin 02

---

![PROTOTYPE_4](images/PROTOTYPE_4-2.jpg)

---

# Prototype 5


 
### Dessin 01

---

![PROTOTYPE_5](images/PORTOTYPE_5-1.jpg)

---

### Dessin 02

---

![prototype](images/PROTOTYPE_5-2.jpg)

---
